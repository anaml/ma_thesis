import h5py
import numpy as np
from pathlib import Path
import torch
import logging
import os
from parsing import parse_args
from secgan_helper import SECGANConfig
from secgan_module import SECGANModule
from elektronn3.inference import Predictor


def inference():
    args = parse_args()
    exp_name = "22-12-14_15-38-24-957814"
    secgan_logger = logging.getLogger('se_cycle_gan_logs_' + exp_name)
    save_path = args.save_path if os.getenv('CLUSTER') == 'WHOLEBRAIN' else os.path.expanduser(args.save_path)
    save_dir = os.path.join(save_path, exp_name + '/')
    save_dir_y2x = os.path.join(save_dir, "Gy2x/")
    os.makedirs(save_dir, exist_ok=True)
    os.makedirs(save_dir_y2x, exist_ok=True)
    print(f"Save dir: {save_dir}")
    secgan_config = SECGANConfig(**vars(args))
    
    model = SECGANModule.load_from_checkpoint('//cajal/u/anaml/SecGAN2/runs/22-12-14_15-38-24-957814/22-12-14_15-38-24-957814/version_0/checkpoints/epoch=19-step=10000.ckpt', config=secgan_config, logging_data=None, print_logger=secgan_logger, save_dir=save_dir_y2x, inference=False) 
    print("Model loaded")
    p_syn_raw = Path('/cajal/scratch/users/anaml/3k_65000n_v3_test/labels_al_rawdata_no_dilation_darkerlight_wborders_new.hdf5') 
    inference_path = Path('/cajal/scratch/users/anaml/3k_65000n_v3_test/tiled_inference_957814/syn_data_22-12-14_15-38-24-957814_v1.hdf5')
    
    hf = h5py.File(str(p_syn_raw), "r")
    cube_raw = hf['label_values'][()]
    print("Loaded syn data")
    x = cube_raw / 255
    x = x.astype(np.float32)[None]
    x = x.astype(np.float32)[None]
    x = torch.Tensor(x)
    
    predictor = Predictor(model=model, state_dict_src=None, device="cuda:0", batch_size=2,
                          tile_shape=(88,88,88), overlap_shape=(36,36,36), offset=None, out_shape = (1,3872,3872,3872),
                          out_dtype=torch.float32, float16=False, apply_softmax=False, transform=None,
                          augmentations=None, strict_shapes=False, apply_argmax=False, argmax_with_threshold=None,
                          verbose=True, report_inp_stats=False)
    
    print("Initialized predictor for inference")
    
    inf_res = predictor.predict(x)
    inf_res = inf_res.numpy()
    inference_res = inf_res[0,0]
    inference_res = inference_res * 255
    inference_res = inference_res.astype(np.uint8)
    
    with h5py.File(str(inference_path), 'w') as f:
        f.create_dataset("label_values", data=inference_res, dtype=np.uint8)
        f.close()

if __name__ == '__main__':
    inference()
