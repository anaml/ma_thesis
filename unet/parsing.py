import argparse
from argparse import ArgumentParser
from constants import BOUNDS_SYNTHETIC, BOUNDS_SYNTHETIC_TRAIN, BOUNDS_SYNTHETIC_VAL, BOUNDS_J0126


def parse_args():
    parser: ArgumentParser = argparse.ArgumentParser(description='Train a 3D UNet.')
    parser.add_argument(
        '-ft', '--fine-tuning', type=bool, default=False,
        help='Set true for finte-tuning(ft)'
    )
    parser.add_argument(
        '-dx', '--data-path-x', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/raw/batch3',
        help='Path to the synthetic EM dataset(train)'
    )
    parser.add_argument(
        '-gtx', '--ground-truth-x', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/unet_labels/batch3',
        help='Path to the ground truth of the synthetic EM data(train)'
    )
    parser.add_argument(
        '-dy', '--data-path-y', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/raw/validation', 
        help='Path to the synthetic EM dataset(validation)'
    )
    parser.add_argument(
        '-gty', '--ground-truth-y', metavar='PATH',
        default='/cajal/scratch/users/anaml/GT/unet_labels/validation', 
        help='Path to the ground truth of the synthetic EM data(validation)'
    )
    parser.add_argument(
        '-me', '--max-epoch', type=int, default=100,
        help='Maximum epoch to train'
    )
    parser.add_argument(
        '-est', '--epoch-size-train', type=int, default=100,
        help='How many training samples to process in a train epoch'
    )
    parser.add_argument(
        '-esv', '--epoch-size-val', type=int, default=10,
        help='How many validation samples to process in a val epoch'
    )
    parser.add_argument(
        '-ms', '--max-steps', type=int, default=100*100,
        help='Max steps for training (nr epochs * len train dataset)'
    )
    parser.add_argument(
        '-sp', '--save-path', metavar='PATH', default='/cajal/nvmescratch/users/anaml/UNet_runs/',
        help='Path to save the model at'
    )
    parser.add_argument(
        '-en', '--experiment-name', default=None,
        help='Experiment name'
    )
    parser.add_argument(
        '-lr', '--learning-rate', type=float, default=0.0001,
        help='Learning rate'
    )
    parser.add_argument(
        '-b', '--batch-size', type=int, default=2, 
        help='Batch size'
    )
    parser.add_argument(
        '-bl', '--batch-size-logging', type=int, default=4,
        help='Batch size for logging the constant samples'
    )
    parser.add_argument(
        '-se', '--seed', type=int, default=0,
        help='Seed for RNGs'
    )
    parser.add_argument(
        '-nc', '--number-classes', type=int, default=1,
        help='Number of classes: foreground and background'
    )
    parser.add_argument(
        '-ic', '--in-channels', type=int, default=1,
        help='Number of input channels'
    )
    parser.add_argument(
        '-oc', '--out-channels', type=int, default=1,
        help='Number of output channels. Depends on number of classes'
    )
    parser.add_argument(
        '-nb', '--n-blocks', type=int, default=4,
        help='Number of blocks'
    )
    parser.add_argument(
        '-sf', '--start-filts', type=int, default=64,
        help='Number of start filters'
    )
    parser.add_argument(
        '-um', '--up-mode', type=str, default='transpose',
        help='Upsampling mode'
    )
    parser.add_argument(
        '-mm', '--merge-mode', type=str, default='concat',
        help='Merge mode'
    )
    parser.add_argument(
        '-pb', '--planar-blocks', type=int, default=(),
        help='Number of input channels'
    )
    parser.add_argument(
        '-bn', '--batch-norm', type=str, default='unset',
        help=''
    )
    parser.add_argument(
        '-att', '--attention', type=bool, default=False,
        help=''
    )
    parser.add_argument(
        '-act', '--activation', nargs= '+', type=str, default='relu',
        help='Name of the non-linear activation. Choices: "relu", "silu", "leaky", "prelu", "rrelu" '
    )
    parser.add_argument(
        '-nn', '--normalization', type=str, default='batch',
        help=''
    )
    parser.add_argument(
        '-cm', '--conv-mode', type=str, default='same',
        help=''
    )
    parser.add_argument(
        '-ps', '--patch-shape', nargs='+', type=int, default=[100, 100, 100], 
        help='The shape of the patches which are sampled, xzy'
    )
    parser.add_argument(
        '-psl', '--patch-shape-logging', nargs='+', type=int, default=[64, 200, 200],
        help='The shape of the patches for tensorboard logging (and ground truth loss calc.) which are sampled, zxy'
    )
    parser.add_argument(
        '-l', '--loss', type=str, default="bce",
        help='Loss to be used. Choose between bce, dice and combined'
    )
    parser.add_argument(
        '-dlw', '--dice-loss-weight', type=float, default=1., 
        help='Weight for the dice loss. 0.5 weights both classes equally'
    )
    parser.add_argument(
        '-clw', '--combined-loss-weight', type=float, default=[0.5, 0.5], 
        help='Weight for the combined loss(bce and dice). 1. weights both losses equally'
    )
    parser.add_argument(
        '-nw', '--num-workers', type=int, default=10,
        help='how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process'
    )
    parser.add_argument(
        '-vb', '--verbose', action='store_true',
        help='ensures logging/printing all statements'
    )
    parser.add_argument(
        '-mst', '--memory-stats', action='store_true',
        help='prints all memory statuses for all devices available'
    )
    parser.add_argument(
        '-tb', '--train-bounds', nargs='+', type=int, default=BOUNDS_J0126,
        help='Bounds for the train'
    )
    parser.add_argument(
        '-valb', '--val-bounds', nargs='+', type=int, default=BOUNDS_J0126,
        help='Bounds for the val'
    )
    parser.add_argument(
        '-wd', '--weight-decay', type=float, default=1e-8,
        help='Weight decay for optimizers'
    )
    parser.add_argument(
        '-o', '--optimizer', type=str, default='Adam',
        help='optimizer'
    )
    parser.add_argument(
        '-pm', '--padding-mode', type=str, default="same",
        help='padding mode for the generator models. One of "valid" or "same"'
    )
    parser.add_argument(
        '-fact', '--final-activation', type=str, default="sigmoid",
        help='what activation function to use in the last layer. One of "sigmoid", "tanh", '
             '"algebraic_sigmoid", "rescale" or "none". Make sure the value range is appropriate'
    )
    parser.add_argument(
        '-nl', '--normalization-layer', type=str, default="batchnorm",
        help='what normalization layer to use. One of "batchnorm" or "groupnorm"'
    )
    parser.add_argument(
        '-gnng', '--groupnorm-num-groups', type=int, default=2,
        help='if normalization_layer="groupnorm", number of groups'
    )
   
    args = parser.parse_args()

    print(args)
    return args

