from typing import Sequence
from elektronn3.data import transforms
from knossos_data import KnossosRawDataSynthetic
import torch
from knossos_data_j0126 import KnossosRawData
from pathlib import Path

def get_transforms(divide=1):
    # Transformations to be applied to samples before feeding them to the network
    common_transforms = [
        transforms.Lambda(lambda x, y: (x / divide, y))
    ]
    
    active_transforms = [transforms.RandomRotate2d(prob=0.6),
                         transforms.RandomFlip(ndim_spatial=3),
                         transforms.RandomGrayAugment(channels=[0], prob=0.4),
                         transforms.RandomGammaCorrection(gamma_std=0.25, gamma_min=0.25, prob=0.5),
                         transforms.AdditiveGaussianNoise(sigma=0.2, channels=[0], prob=0.6)
                        ]
    
    train_transform = transforms.Compose(active_transforms + common_transforms)
    valid_transform = transforms.Compose(common_transforms)

    return train_transform, valid_transform


class UNETDataConfig:
    def __init__(self,
                 data_path_x: str,
                 ground_truth_x: str,
                 data_path_y:str,
                 ground_truth_y: str,
                 train_bounds: Sequence[Sequence[int]],
                 val_bounds: Sequence[Sequence[int]],
                 batch_size=2,
                 batch_size_logging=2,
                 epoch_size_train=500,
                 epoch_size_val=20,
                 num_workers=5,
                 patch_shape=(88, 88, 88),
                 patch_shape_logging=(64, 150, 150),
                 verbose=False,
                 **kwargs
                 ):
        """
        The configuration for the SECGAN Data
        :param batch_size: The batch size for training
        :param batch_size_logging: The batch size for logging
        :param epoch_size_train: The training epoch size
        :param epoch_size_val: The validation epoch size
        :param num_workers: The number of workers per dataloader
        :param patch_shape: The patch shape for training/evaluation
        :param patch_shape_logging: The patch shape for logging
        :param train_bounds: The bounds of the training patches for Y (in case GT is available)
        :param valid_bounds: The bounds of the validation patches for Y (in case GT is available)
        :param verbose: Whether to be verbose
        """
        self.data_path_x = data_path_x
        self.ground_truth_x = ground_truth_x
        self.data_path_y = data_path_y
        self.ground_truth_y = ground_truth_y
        self.train_bounds = train_bounds
        self.val_bounds = val_bounds
        self.batch_size = batch_size
        self.batch_size_logging = batch_size_logging
        self.epoch_size_train = epoch_size_train
        self.epoch_size_val = epoch_size_val
        self.num_workers = num_workers
        self.patch_shape = patch_shape
        self.patch_shape_logging = patch_shape_logging
        self.verbose = verbose

    
def tuple2bounds(coordinate_list):
    return coordinate_list[:3], coordinate_list[3:]

class ConcatDataset(torch.utils.data.Dataset):
    # https://discuss.pytorch.org/t/train-simultaneously-on-two-datasets/649/2

    def __init__(self, *datasets):
        self.datasets = datasets

    def __getitem__(self, i):
        return tuple(d[i] for d in self.datasets)

    def __len__(self):
        return min(len(d) for d in self.datasets)

def setup_data(args: UNETDataConfig):
    train_transform, valid_transform = get_transforms(divide=255)

    print('Loading datasets (this might take a while)')
    
    # train_dataset= KnossosRawDataSynthetic(data_path_x=args.data_path_x,
    #                                        gt_path_x=args.ground_truth_x,
    #                                        patch_shape=args.patch_shape,  # [z]yx
    #                                        transform=train_transform,
    #                                        bounds=tuple2bounds(args.train_bounds),  # xyz
    #                                        epoch_size=args.epoch_size_train,
    #                                        disable_memory_check=False,
    #                                        verbose=args.verbose)
    
    # valid_dataset = KnossosRawDataSynthetic(data_path_x=args.data_path_y,
    #                                         gt_path_x=args.ground_truth_y,
    #                                         patch_shape=args.patch_shape,
    #                                         transform=valid_transform,
    #                                         bounds=tuple2bounds(args.val_bounds),  # xyz
    #                                         epoch_size=args.epoch_size_val,
    #                                         disable_memory_check=False,
    #                                         verbose=args.verbose)
    
    
    data_path_train = Path(args.data_path_x)
    raw_files_train = list(data_path_train.iterdir())
    raw_h5 = [f for f in raw_files_train if str(f).endswith(".h5")]
    raw_h5_train = raw_h5[0:1] 
    
    gt_path_train = Path(args.ground_truth_x)
    unet_labels_train = list(gt_path_train.iterdir())
    u_labels_train = unet_labels_train[0:1]
    
    data_path_val = Path(args.data_path_y)
    raw_files_val = list(data_path_val.iterdir())
    raw_h5 = [f for f in raw_files_val if str(f).endswith(".h5")]
    raw_h5_val = raw_h5[0:1]
    
    gt_path_val = Path(args.ground_truth_y)
    unet_labels_val = list(gt_path_val.iterdir())
    u_labels_val = unet_labels_val[0:1]
    
    
    train_dataset = KnossosRawData(data_path_x=raw_h5_train,
                                  gt_path_x=u_labels_train,
                                  patch_shape=args.patch_shape,  # [z]yx
                                  transform=train_transform,
                                  bounds=tuple2bounds(args.train_bounds),  # xyz
                                  epoch_size=args.epoch_size_train,
                                  disable_memory_check=False,
                                  verbose=args.verbose)
    
    valid_dataset = KnossosRawData(data_path_x=raw_h5_val,
                                  gt_path_x=u_labels_val,
                                  patch_shape=args.patch_shape,  # [z]yx
                                  transform=valid_transform,
                                  bounds=tuple2bounds(args.val_bounds),  # xyz
                                  epoch_size=args.epoch_size_val,
                                  disable_memory_check=False,
                                  verbose=args.verbose)
    
    
    
    return train_dataset, valid_dataset    