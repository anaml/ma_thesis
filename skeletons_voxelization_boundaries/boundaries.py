import numba
import numpy as np
import scipy.ndimage.filters as filters
from pathlib import Path
import h5py
import time

vx_rng = 3000
labels_path = Path('/cajal/scratch/users/anaml/3k_50000n_v2/labels_al.hdf5')
labels_path_reshaped = Path('/cajal/scratch/users/anaml/3k_50000n_v2/labels_al_4fold_dilation_light_wborders_new.hdf5') 
hf1 = h5py.File(str(labels_path), "r")
borders_path = Path('/cajal/scratch/users/anaml/3k_50000n_v2/labels_al_rawdata_4fold_dilation_light_wborders_new.hdf5')

'''
Taken from: https://github.com/ELEKTRONN/ELEKTRONN2/blob/master/elektronn2/data/image.py#L182
'''
@numba.jit(nopython=True)
def _ids2barriers(ids, barriers, dilute, connectivity):
    """
    Draw a 2 or 4 pix barrier where label IDs are different
    :param ids:  (x,y,z)
    :param barriers:
    :param dilute: e.g. [False, True, True]
    :param connectivity: e.g. [True, True, True]
    :return:
    """
    nx = ids.shape[0]
    ny = ids.shape[1]
    nz = ids.shape[2]

    for x in np.arange(nx - 1):
        for y in np.arange(ny - 1):
            for z in np.arange(nz - 1):
                if connectivity[0]:
                    if ids[x, y, z] != ids[x + 1, y, z]:
                        barriers[x, y, z] = 1
                        barriers[x + 1, y, z] = 1
                        if dilute[0]:
                            if x > 0:    barriers[x - 1, y, z] = 1
                            if x < nx - 2: barriers[x + 2, y, z] = 1
                            if x < nx - 3: barriers[x + 3, y, z] = 1
                            if x < nx - 4: barriers[x + 4, y, z] = 1
                            if x < nx - 5: barriers[x + 5, y, z] = 1

                if connectivity[1]:
                    if ids[x, y, z] != ids[x, y + 1, z]:
                        barriers[x, y, z] = 1
                        barriers[x, y + 1, z] = 1
                        if dilute[1]:
                            if y > 0:    barriers[x, y - 1, z] = 1
                            if y < ny - 2: barriers[x, y + 2, z] = 1
                            if y < ny - 3: barriers[x, y + 3, z] = 1
                            if y < ny - 4: barriers[x, y + 4, z] = 1
                            if y < ny - 5: barriers[x, y + 5, z] = 1

                if connectivity[2]:
                    if ids[x, y, z] != ids[x, y, z + 1]:
                        barriers[x, y, z] = 1
                        barriers[x, y, z + 1] = 1
                        if dilute[2]:
                            if z > 0:    barriers[x, y, z - 1] = 1
                            if z < nz - 2: barriers[x, y, z + 2] = 1
                            if z < nz - 3: barriers[x, y, z + 3] = 1
                            if z < nz - 4: barriers[x, y, z + 4] = 1
                            if z < nz - 5: barriers[x, y, z + 5] = 1


def ids2barriers(ids, dilute=[True, True, True],
                 connectivity=[True, True, True],
                 ecs_as_barr='new_class',
                 smoothen=True):
    dilute = np.array(dilute)
    connectivity = np.array(connectivity)
    barriers = np.zeros_like(ids, dtype=np.int16)

    _ids2barriers(ids, barriers, dilute, connectivity)
    _ids2barriers(ids[::-1, ::-1, ::-1],
                  barriers[::-1, ::-1, ::-1],
                  dilute, connectivity)  # apply backwards as lazy hack to fix boundary

    if smoothen:
        kernel = np.array([[[0.1, 0.2, 0.1],
                            [0.2, 0.3, 0.2],
                            [0.1, 0.2, 0.1]],

                           [[0.3, 0.5, 0.3],
                            [0.5, 1.0, 0.5],
                            [0.3, 0.5, 0.3]],

                           [[0.1, 0.2, 0.1],
                            [0.2, 0.3, 0.2],
                            [0.1, 0.2, 0.1]]])

        barriers_s = filters.convolve(barriers.astype(np.float32),
                                      kernel.astype(np.float32))
        barriers = (barriers_s > 4).astype(np.int16) 
       

    if ecs_as_barr == 'new_class':
        ecs = np.logical_and((ids == 0), (barriers != 1))
        barriers[ecs] = 2

    elif ecs_as_barr:
        ecs = (ids == 0).astype(np.int16)
        barriers = np.maximum(ecs, barriers)

    return barriers


if __name__ == '__main__':
    s = time.time()
    print("Loading data set")
    ids = hf1['label_values'][()]
    print("Computing barriers")
    barriers = ids2barriers(ids)
    print("Barriers computed")
    print("Coloring")    
    barriers[barriers == 0] = 140
    barriers[barriers == 1] = 0
    barriers[barriers == 2] = 0
    print("Colored")
    print("Computing boundaries for new empty space created")
    barriers2 = ids2barriers(barriers, dilute=[False, False, False])
    print("Computed")
    print("Coloring again")
    barriers2[barriers2 == 0] = 140 
    barriers2[barriers2 == 1] = 110
    barriers2[barriers2 == 2] = 160
    print("Colored")
    print("Reassigning ids and writing to disk")
    ids[barriers2 == 160] = 0
    with h5py.File(str(labels_path_reshaped), 'w') as f:
        f.create_dataset("label_values", data=ids, dtype=np.uint16)
        f.close()
    print("Reassigned and saved")
    print("Writing to disk")
    with h5py.File(str(borders_path), 'w') as f:
        f.create_dataset("label_values", data=barriers2, dtype=np.uint8)
        f.close()
    print("Saved")
    e = time.time() - s
    print(f"took: {e} seconds")
