import random
from scipy.spatial import Voronoi, cKDTree
import numpy as np
import time
import math
import h5py
import pickle
from tqdm import tqdm
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor

rng = np.random.default_rng()
voxel_rng = 3000 
x1, x2 = 0, voxel_rng
y1, y2 = 0, voxel_rng
z1, z2 = 0, voxel_rng
nr_vorpts = 600000
nr_neurons = 50000

sample = rng.uniform([x1, y1, z1], [x2, y2, z2], size=(nr_vorpts, 3))
sample = np.float32(sample)
IDs = np.zeros(sample.shape[0], dtype=np.uint16)
curr_id = 0
labels_path = Path('/cajal/scratch/users/anaml/labels_al_flat.hdf5')
neurons_path = Path('/cajal/scratch/users/anaml/neurons.hdf5')
ids_path = Path('/cajal/scratch/users/anaml/ids.pickle')

with h5py.File(str(labels_path), 'w') as f:
    f.create_dataset("label_values", shape=(voxel_rng * voxel_rng * voxel_rng,), maxshape=(None,), dtype=np.uint16)
    f.close()


def create_slices():
    slice_idx = []
    arr_idx = []
    nr_loops = voxel_rng * voxel_rng
    i_coord_cnt = 0
    j_coord_cnt = 0
    i_coord = np.empty(voxel_rng, dtype=np.uint16)
    j_coord = np.empty(voxel_rng, dtype=np.uint16)
    j_coord.fill(j_coord_cnt)
    k_coord = np.arange(voxel_rng, dtype=np.uint16)
    for _ in tqdm(range(nr_loops)):
        i_coord.fill(i_coord_cnt)
        start = i_coord_cnt * voxel_rng * voxel_rng + j_coord_cnt * voxel_rng
        end = start + voxel_rng
        arr = np.array([i_coord, j_coord, k_coord], dtype=np.uint16).T
        slice_idx.append(np.s_[start:end])
        arr_idx.append(arr)
        i_coord_cnt += 1
        if i_coord_cnt == voxel_rng:
            j_coord_cnt += 1
            j_coord.fill(j_coord_cnt)
            i_coord_cnt = 0
    return slice_idx, arr_idx


# @profile
def pool_func(arr):
    print(arr[0])
    _, idx = kd.query(arr, k=1)
    ordered_ids = IDs[idx]
    return ordered_ids


class ConnectivityGraph:
    def __init__(self, points, n):
        self.n = n
        self.vor = Voronoi(points)
        self.connections = self.vor.ridge_points
        self.marked_connections = np.zeros(self.n)
        self.adj = self.create_adj()
        
    def create_adj(self):
        adj = np.zeros((self.n, self.n))
        for [row, col] in self.connections:
            adj[row, col] = adj[col, row] = 1

        return adj
    
    def check_marked(self, idx):
        return self.marked_connections[idx] == 1

    def compute_line_locations(self, connections):
        line_locations = []
        for [pt1, pt2] in connections:
            line_locations.append([self.vor.points[pt1], self.vor.points[pt2]])
        return line_locations

    def choose_next_node(self, idx, s):
        min_angle = math.pi
        index = idx[0]
        vec1 = self.vor.points[s]
        for i in idx:
            vec2 = self.vor.points[i]
            angle = np.arccos(np.dot(vec1 / np.linalg.norm(vec1), vec2 / np.linalg.norm(vec2)))
            if angle < min_angle and not self.check_marked(i):
                min_angle = angle
                index = i

        return index
    
    def create_main_branch(self, s, len=15):  
        if s is None:
            return []
        branch = [s]
        self.marked_connections[s] = 1
        IDs[s] = curr_id
        for i in range(len):
            idx = np.where(self.adj[s] > 0)[0]
            if idx.shape[0] == 0:
                return branch
            next = self.choose_next_node(idx,s)
            if not self.check_marked(next):
                branch.append(next)
                s = next
                self.marked_connections[s] = 1
                IDs[s] = curr_id

        return branch
    
    def create_lines(self, branch):
        if len(branch) < 2:
            raise ValueError("Should not be able to generate branches this small")
        connections = []
        for i in range(len(branch) - 1):
            connections.append([branch[i], branch[i + 1]])
        return self.compute_line_locations(np.asarray(connections))
    
    def create_lines_branching_system(self, branches):
        connections = []
        for branch in branches:
            if len(branch) >= 2:
                connections.extend(self.create_lines(branch))
        return connections
    
    def start_node(self):
        if np.all(self.marked_connections == 1):
            return None
        s = random.randint(0, self.n - 1)
        while self.marked_connections[s] == 1:
            s = random.randint(0, self.n - 1)
        return s
    
    def create_branching_system(self, sampled_pts):
        lines = []

        p = random.random()
        if p > 0.5:
            sampled_pts.pop(0)

        for point in sampled_pts:
            result = self.create_main_branch(point, len=5)
            if result:
                lines.append(result)
        return lines

# @profile
def save_vor_points(pts):
    f = h5py.File(str(neurons_path), 'w')
    f.create_dataset("vorpts", data=pts)
    f.close()

# @profile
def save_neurons(main_branch, small_branches, nr_neuron):
    f = h5py.File(str(neurons_path), 'a')
    f.create_dataset(f"n_{nr_neuron}_main", data=main_branch)
    f.create_dataset(f"n_{nr_neuron}_branches", data=small_branches)
    f.close()

# @profile
def save_labels(v):
    f = h5py.File(str(neurons_path), 'a')
    f.create_dataset(f"color_labels", data=v)
    f.close()


if __name__ == '__main__':
    start = time.time()
    connectivity_graph = ConnectivityGraph(sample, nr_vorpts)
    r = connectivity_graph.vor.ridge_vertices
    save_vor_points(connectivity_graph.vor.points)

    for i in range(nr_neurons):
        curr_id += 1
        branch1 = connectivity_graph.create_main_branch(connectivity_graph.start_node())
        if len(branch1) <= 2:
            IDs[IDs == curr_id] = 0
            continue
        lines2 = connectivity_graph.create_lines(branch1)
        lines2arr = np.asarray(lines2)
        branches = connectivity_graph.create_branching_system(branch1)
        branches = connectivity_graph.create_lines_branching_system(branches)
        branchesarr = np.asarray(branches)
        if branches:
            save_neurons(lines2arr, branchesarr, i)
        else:
            IDs[IDs == curr_id] = 0

    with open(str(ids_path), 'wb') as h:
        pickle.dump(IDs, h, protocol=pickle.HIGHEST_PROTOCOL)

    print(f"Max id is: {max(IDs)}")
    slice_idx, arr_idx = create_slices()
    print("Computed slices")
    kd = cKDTree(sample)
    print("Computed kd tree")
    hf1 = h5py.File(str(labels_path), "a")
    print("Opening file for writing")

    with ProcessPoolExecutor(max_workers=10) as executor:
        res = executor.map(pool_func, arr_idx)

    print("Computed voxelization")
    for sl, idx in tqdm(zip(res, slice_idx), total=len(slice_idx)):
        hf1['label_values'][idx] = sl
    hf1.close()
    end = time.time() - start
    print(f"took {end} seconds to run the script")