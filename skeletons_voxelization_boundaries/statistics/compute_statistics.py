import os
from pathlib import Path
import h5py
import numpy as np
from collections import defaultdict
import numba
import scipy.ndimage.filters as filters
import pickle
import json

save_gt = Path('/cajal/scratch/users/anaml/3k_50000n/statistics_gt.pickle')
save_txt = Path('/cajal/scratch/users/anaml/3k_50000n/statistics_gt.txt')

nr_diff_neurons = []
nr_surface_vx = []
foreground_vx = []
gini_coeffs = []

cnt = 0
nr_cubes = 19
cube_size = 150
total_vx = cube_size * cube_size * cube_size


def gini(x):
    total = 0
    for i, xi in enumerate(x[:-1], 1):
        total += np.sum(np.abs(xi - x[i:]))
    return total / (len(x)**2 * np.mean(x))


def calculate_share_vx_per_idx(fp):
    f = h5py.File(fp, 'r')
    labels = f['labels']
    values, counts = np.unique(labels, return_counts=True)
    f.close()
    return counts


def calc_gini_coeffs(fp):
    counts = calculate_share_vx_per_idx(fp)
    gini_c = gini(counts)
    gini_coeffs.append(gini_c)


def calculate_unique_nr_of_neurons(fp):
    f = h5py.File(fp, 'r')
    labels = f['labels']
    diff= len(np.unique(labels))
    nr_diff_neurons.append(diff)
    f.close()

'''
Taken from: https://github.com/ELEKTRONN/ELEKTRONN2/blob/master/elektronn2/data/image.py#L182
'''
@numba.jit(nopython=True)
def _ids2barriers(ids, barriers, dilute, connectivity):
    """
    Draw a 2 or 4 pix barrier where label IDs are different
    :param ids:  (x,y,z)
    :param barriers:
    :param dilute: e.g. [False, True, True]
    :param connectivity: e.g. [True, True, True]
    :return:
    """
    nx = ids.shape[0]
    ny = ids.shape[1]
    nz = ids.shape[2]

    for x in np.arange(nx - 1):
        for y in np.arange(ny - 1):
            for z in np.arange(nz - 1):
                if connectivity[0]:
                    if ids[x, y, z] != ids[x + 1, y, z]:
                        barriers[x, y, z] = 1
                        barriers[x + 1, y, z] = 1
                        if dilute[0]:
                            if x > 0:    barriers[x - 1, y, z] = 1
                            if x < nx - 2: barriers[x + 2, y, z] = 1

                if connectivity[1]:
                    if ids[x, y, z] != ids[x, y + 1, z]:
                        barriers[x, y, z] = 1
                        barriers[x, y + 1, z] = 1
                        if dilute[1]:
                            if y > 0:    barriers[x, y - 1, z] = 1
                            if y < ny - 2: barriers[x, y + 2, z] = 1

                if connectivity[2]:
                    if ids[x, y, z] != ids[x, y, z + 1]:
                        barriers[x, y, z] = 1
                        barriers[x, y, z + 1] = 1
                        if dilute[2]:
                            if z > 0:    barriers[x, y, z - 1] = 1
                            if z < nz - 2: barriers[x, y, z + 2] = 1


def ids2barriers(ids, dilute=[False, False, False],
                 connectivity=[True, True, True],
                 ecs_as_barr='new_class',
                 smoothen=False):
    dilute = np.array(dilute)
    connectivity = np.array(connectivity)
    barriers = np.zeros_like(ids, dtype=np.int16)

    _ids2barriers(ids, barriers, dilute, connectivity)
    _ids2barriers(ids[::-1, ::-1, ::-1],
                  barriers[::-1, ::-1, ::-1],
                  dilute, connectivity)  # apply backwards as lazy hack to fix boundary

    if smoothen:
        kernel = np.array([[[0.1, 0.2, 0.1],
                            [0.2, 0.3, 0.2],
                            [0.1, 0.2, 0.1]],

                           [[0.3, 0.5, 0.3],
                            [0.5, 1.0, 0.5],
                            [0.3, 0.5, 0.3]],

                           [[0.1, 0.2, 0.1],
                            [0.2, 0.3, 0.2],
                            [0.1, 0.2, 0.1]]])

        barriers_s = filters.convolve(barriers.astype(np.float32),
                                      kernel.astype(np.float32))
        barriers = (barriers_s > 4).astype(np.int16)  # (old - new).mean() ~ 0

    if ecs_as_barr == 'new_class':
        ecs = np.logical_and((ids == 0), (barriers != 1))
        barriers[ecs] = 2

    elif ecs_as_barr:
        ecs = (ids == 0).astype(np.int16)
        barriers = np.maximum(ecs, barriers)

    return barriers


def calculate_surface_vx(fp):
    f = h5py.File(fp, 'r')
    labels = f['labels'][()]
    barriers = ids2barriers(labels)
    unique, counts = np.unique(barriers, return_counts=True)
    dic = dict(zip(unique, counts))
    nr_surface_vx.append(dic[1])
    f.close()


def calculate_foreground_vx(fp):
    f = h5py.File(fp, 'r')
    labels = f['labels']
    values, counts = np.unique(labels, return_counts=True)
    d = dict(zip(values, counts))
    fore = total_vx - d[0]
    foreground_vx.append(fore)
    f.close()


if __name__ == '__main__':

    path = Path('/wholebrain/fromhd/jkor/from_lustre/new j0126 segmentation gt/batch2')
    os.chdir(str(path))

    for file in os.listdir():
        if file.endswith(".h5"):
            file_path = os.path.join(str(path), file)
            calculate_unique_nr_of_neurons(file_path)
            calculate_surface_vx(file_path)
            calculate_foreground_vx(file_path)
            calc_gini_coeffs(file_path)
            cnt += 1

    path = Path('/wholebrain/fromhd/jkor/from_lustre/new j0126 segmentation gt/batch1')
    os.chdir(str(path))

    for file in os.listdir():
        if file.endswith(".h5"):
            file_path = os.path.join(str(path), file)
            calculate_unique_nr_of_neurons(file_path)
            calculate_surface_vx(file_path)
            calculate_foreground_vx(file_path)
            calc_gini_coeffs(file_path)
            cnt += 1
            if cnt == nr_cubes:
                break

    res_gt = defaultdict()

    mean = np.mean(nr_diff_neurons)
    std = np.std(nr_diff_neurons)
    res_gt['mean'] = mean
    res_gt['std'] = std

    mean = np.mean(nr_surface_vx)
    std = np.std(nr_surface_vx)
    res_gt['mean_surface_vx'] = mean
    res_gt['std_surface_vx'] = std

    mean = np.mean(foreground_vx)
    std = np.std(foreground_vx)
    res_gt['mean_volume'] = mean
    res_gt['std_volume'] = std

    mean = np.mean(gini_coeffs)
    std = np.std(gini_coeffs)
    res_gt['mean_volume_ineq'] = mean
    res_gt['std_volume_ineq'] = std

    with open(str(save_gt), 'wb') as h:
        pickle.dump(res_gt, h, protocol=pickle.HIGHEST_PROTOCOL)

    with open(str(save_txt), 'w') as file:
        file.write(json.dumps(res_gt))
