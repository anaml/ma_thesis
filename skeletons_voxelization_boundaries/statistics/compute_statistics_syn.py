import h5py
import numpy as np
from pathlib import Path
from tqdm import tqdm
from collections import defaultdict
import pickle
from PIL import Image
import json

p_syn = Path('/cajal/scratch/users/anaml/3k_50000n/labels_al.hdf5')
p_syn_raw = Path('/cajal/scratch/users/anaml/3k_50000n/labels_al_rawdata_4fold_dilation_light_wborders_new.hdf5')
save = Path('/cajal/scratch/users/anaml/3k_50000n/statistics_syn.pickle')
save_txt = Path('/cajal/scratch/users/anaml/3k_50000n/statistics_syn.txt')
save_im_path = Path('/cajal/scratch/users/anaml/3k_50000n/surface_vx_syn_150.jpg')

hf1 = h5py.File(str(p_syn), "r")
hf2 = h5py.File(str(p_syn_raw), "r")
cube_size = 150
nr_cubes = 19
total_vx = cube_size * cube_size * cube_size

nr_diff_neurons = []
nr_surface_vx = []
foreground_vx = []
gini_coeffs = []


def gini(x):
    total = 0
    for i, xi in enumerate(x[:-1], 1):
        total += np.sum(np.abs(xi - x[i:]))
    return total / (len(x)**2 * np.mean(x))


if __name__ == '__main__':
    cube = hf1['label_values'][()]
    cube_raw = hf2['label_values'][()]
    hf1.close()
    hf2.close()
    st = 0
    en = cube_size

    for _ in tqdm(range(nr_cubes)):
        cube_small = cube[st:en, st:en, st:en]
        cube_small_raw = cube_raw[st:en, st:en, st:en]
        diff = len(np.unique(cube_small))
        nr_diff_neurons.append(diff)

        values, counts = np.unique(cube_small, return_counts=True)
        d = dict(zip(values, counts))
        fore = total_vx - d[0]
        foreground_vx.append(fore)

        gini_c = gini(counts)
        gini_coeffs.append(gini_c)

        unique, counts = np.unique(cube_small_raw, return_counts=True)
        dic = dict(zip(unique, counts))
        nr_surface_vx.append(dic[110])

        st = en + 1
        en = st + cube_size
    
    res_syn = defaultdict()

    c = cube_raw[0:150, 0:150, 0:150]
    im = Image.fromarray(c[0])
    im.save(str(save_im_path))

    mean = np.mean(nr_diff_neurons)
    std = np.std(nr_diff_neurons)
    res_syn['mean'] = mean
    res_syn['std'] = std

    mean = np.mean(nr_surface_vx)
    std = np.std(nr_surface_vx)
    res_syn['mean_surface_vx'] = mean
    res_syn['std_surface_vx'] = std

    mean = np.mean(foreground_vx)
    std = np.std(foreground_vx)
    res_syn['mean_volume'] = mean
    res_syn['std_volume'] = std

    mean = np.mean(gini_coeffs)
    std = np.std(gini_coeffs)
    res_syn['mean_volume_ineq'] = mean
    res_syn['std_volume_ineq'] = std

    with open(str(save), 'wb') as h:
        pickle.dump(res_syn, h, protocol=pickle.HIGHEST_PROTOCOL)

    with open(str(save_txt), 'w') as file:
        file.write(json.dumps(res_syn))
