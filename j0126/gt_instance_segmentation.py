from skimage.measure import label
from pathlib import Path
import h5py
import os
import numpy as np

model_path = '23-05-02_10-29-20-297445'
batch = 'test'

unet_segm_path = Path(f'/cajal/scratch/users/anaml/GT/tiled_inference_unet/{model_path}/{batch}')
save_path_instance_segm = Path(f'/cajal/scratch/users/anaml/GT/instance_segm/{model_path}/{batch}')
os.makedirs(save_path_instance_segm, exist_ok=True)
ids_path = Path(f'/cajal/scratch/users/anaml/GT/labels/{batch}')
print(save_path_instance_segm)
files = list(ids_path.iterdir())
for file in files:
    fp = str(file)
    if fp.endswith(".h5"):
        sp = os.path.join(str(unet_segm_path), fp.split('/')[-1])
        sp2 = os.path.join(str(ids_path), fp.split('/')[-1])
        hf1 = h5py.File(str(sp), "r")
        hf2 = h5py.File(str(sp2), "r")
        segm = hf1['label_values'][()]
        ids = hf2['label_values'][()]
        unique, counts = np.unique(ids, return_counts=True)
        hf1.close()
        hf2.close()
        segm = segm[5:145, 5:145, 5:145]
        instance_segm, num = label(segm, background=None, return_num=True, connectivity=None)
        sp3 = os.path.join(str(save_path_instance_segm), fp.split('/')[-1])
        with h5py.File(str(sp3), 'w') as f:
            f.create_dataset("label_values", data=instance_segm, dtype=np.uint64)
            f.close()

