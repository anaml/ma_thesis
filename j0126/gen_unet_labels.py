import os
from pathlib import Path
import numpy as np
import h5py
from PIL import Image


save_path = Path("/cajal/scratch/users/anaml/GT/unet_labels/batch1")
save_path2 = Path("/cajal/scratch/users/anaml/GT/unet_labels/batch2")
path = Path('/cajal/scratch/users/anaml/GT/input/batch1')
os.makedirs(save_path, exist_ok=True)
os.makedirs(save_path2, exist_ok=True)
os.chdir(str(path))

print("Processing batch1")
for file in os.listdir():
    if file.endswith(".h5"):
        fp = os.path.join(str(path), file)
        f = h5py.File(fp, 'r')
        barriers = f['label_values'][()]
        print("Loaded cube")
        print(f"Shape: {barriers.shape}")
        barriers[barriers == 140] = 1
        barriers[barriers == 110] = 1
        barriers[barriers == 150] = 0
        sp = os.path.join(str(save_path), file)
        with h5py.File(str(sp), 'w') as hf:
            hf.create_dataset("label_values", data=barriers, dtype=np.uint8)
            hf.close()
        

path = Path('/cajal/scratch/users/anaml/GT/input/batch2')
os.chdir(str(path))
test_path =  Path('/cajal/scratch/users/anaml/GT/input/batch2/test_image.jpg')
test_path2 =  Path('/cajal/scratch/users/anaml/GT/unet_labels/batch2/test_image.jpg')

print("Processing batch2")
for file in os.listdir():
    if file.endswith(".h5"):
        fp = os.path.join(str(path), file)
        f = h5py.File(fp, 'r')
        barriers = f['label_values'][()]
        print("Loaded cube")
        print(f"Shape: {barriers.shape}")
        cc = barriers[0]
        im = Image.fromarray(np.uint8(cc))
        im.save(str(test_path))
        barriers[barriers == 140] = 1
        barriers[barriers == 110] = 1
        barriers[barriers == 150] = 0
        cc = barriers[0]
        cc[cc == 1] = 255
        im = Image.fromarray(np.uint8(cc))
        im.save(str(test_path2))
        sp = os.path.join(str(save_path2), file)
        with h5py.File(str(sp), 'w') as hf:
            hf.create_dataset("label_values", data=barriers, dtype=np.uint8)
            hf.close()