# Synthetic EM Data

This is the implementation of my Masters' Thesis entitled Synthetic Neuron Instance Segmentation Factory. A schematic overview is present in the figure below (a. Represents how we generated the synthetic EM data; b. Shows how we can evaluate the synthetic EM data; c. Depicts how we can utilize synthetic EM data for pre-training, then fine-tune on real VEM data and finally, evaluate the result).
![alt text](/figures/pipeline2.jpg)
## Getting started
Relevant code for generating neuron skeletons, then voxelizing the volume and finally creating synthetic segmentations (steps I and II) can be found in the `skeletons_voxelization_boundaries` folder. This can be achieved with the following commands: 
```
python \skeletons_voxelization_boundaries\connectivity_graph_parallel.py
python \skeletons_voxelization_boundaries\boundaries.py
```
The code for training a SecGAN [[1]](#1) (step III) is listed in the `secgan` folder and can be run with the following command: 
```
python \secgan\cycle_gan.py --load-segmenter /path/to/segmentation_model --save-path /path/to/save/directory/
```
Inferring a SecGAN can be done with: 
```
python \secgan\tiled_inference.py
```

The code for training a 3D UNet [[2]](#2), generating instance segmentations and evaluating the instance segmentation performance can be found in the `unet` and `evaluate` folders (step IV). The `j0126` folder provides the code that was used to perform the evaluation by utilizing real Volume Electron Microscopy data.
Training a UNet can be done with:
```
python \unet\train_unet.py --save-path /path/to/save/directory/
```
Inferring a UNet can be achieved by: 
```
python \unet\tiled_inference.py
```
Generating instance segmentations from semantic segmentations can be achieved by:
```
python \evaluate\instance_segmentation.py
```
Computing the Variation of Information (VoI) score can be done by running the following command:
```
python \evaluate\voi.py
```
## Requirements
This project requires to have the [elektronn3](https://github.com/ELEKTRONN/elektronn3) library installed as well as the packages listed in the `requirements.txt` file by running: 
```
pip install -r requirements.txt
```
## References
<a id="1">[1]</a> Januszewski, M. and Jain, V., 2019. Segmentation-enhanced cyclegan. bioRxiv, p.548081.

<a id="2">[2]</a> Çiçek, Ö., Abdulkadir, A., Lienkamp, S.S., Brox, T. and Ronneberger, O., 2016. 3D U-Net: learning dense volumetric segmentation from sparse annotation. In Medical Image Computing and Computer-Assisted Intervention–MICCAI 2016: 19th International Conference, Athens, Greece, October 17-21, 2016, Proceedings, Part II 19 (pp. 424-432). Springer International Publishing.
