import numpy as np
from pathlib import Path
import h5py

borders_path=Path('/cajal/scratch/users/anaml/3k_65000n_v3_test/labels_al_rawdata_4fold_dilation_darkerlight_wborders_new.hdf5')
labels_path=Path('/cajal/scratch/users/anaml/3k_65000n_v3_test/unet_labels/labels_957814_v1.hdf5')

print("Reading data")
hf1 = h5py.File(str(borders_path), "r")
segm = hf1['label_values'][()]
hf1.close()
print("Done")
print("Labeling data")
segm[segm == 110] = 1
segm[segm == 140] = 1
segm[segm == 160] = 0
print("Done")
print("Saving to disk")
with h5py.File(str(labels_path), 'w') as f:
    f.create_dataset("label_values", data=segm, dtype=np.uint8)
    f.close()
print("Done")    