from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy as np
'''
rand_voi.pyx, rand_voi.hpp, setup.py taken from: https://github.com/funkelab/funlib.evaluate/tree/master/funlib/evaluate
and: https://github.com/funkelab/funlib.evaluate/blob/master/setup.py
'''
setup(
        ext_modules=cythonize([
            Extension(
                'rand_voi',
                sources=[
                    '/cajal/u/anaml/evaluate/rand_voi.pyx'
                ],
                extra_compile_args=['-O3', '-std=c++11'],
                include_dirs=[np.get_include()],
                language='c++')
        ])
)