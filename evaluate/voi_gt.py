import sys
import rand_voi
from pathlib import Path
import h5py
import numpy as np
import os


sys.path.insert(1, '/cajal/u/anaml/evaluate')
batch = 'test'
model = '23-05-02_10-29-20-297445'
p_gen_instance_seg = Path(f'/cajal/scratch/users/anaml/GT/instance_segm/{model}/{batch}')
ids_path = Path(f'/cajal/scratch/users/anaml/GT/labels/{batch}')

files = list(ids_path.iterdir())
for file in files:
    fp = str(file)
    if fp.endswith(".h5"):
        print(fp)
        hf1 = h5py.File(str(fp), "r")
        ids = hf1['label_values'][()]
        unique, counts = np.unique(ids, return_counts=True)
        ids = ids[5:145, 5:145, 5:145]
        hf1.close()
        sp = os.path.join(str(p_gen_instance_seg), fp.split('/')[-1])
        hf_gen = h5py.File(str(sp), "r")
        gen = hf_gen['label_values'][()]
        hf_gen.close()
        print("Running VOI")
        metrics = rand_voi.rand_voi(ids, gen)
        print("Done")
        print(f"Metrics: {metrics}")
        print(f"Total VOI: {metrics['voi_split'] + metrics['voi_merge']}")
