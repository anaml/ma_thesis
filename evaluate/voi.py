import sys
import rand_voi
from pathlib import Path
import h5py

sys.path.insert(1, '/cajal/u/anaml/evaluate')
met_nr = 36
version = 'v25'
nr = '931832'
p_gt_instance_seg = Path("/cajal/scratch/users/anaml/3k_65000n_v3_test/labels_al_4fold_dilation_darkerlight_wborders_new_32b.hdf5")
p_gen_instance_seg = Path(f'/cajal/scratch/users/anaml/3k_65000n_v3_test/instance_segm_957814/instance_segm_22-12-14_15-38-24-957814_{version}_{nr}.hdf5')

print("Loading instance segm GT")
hf_gt = h5py.File(str(p_gt_instance_seg), "r")
gt = hf_gt['label_values'][()]
hf_gt.close()
print("Loaded instance segm GT")

print("Loading instance segm generated")
hf_gen = h5py.File(str(p_gen_instance_seg), "r")
gen = hf_gen['label_values'][()]
print("Loaded instance segm generated")
print(f"Shape generated:{gen.shape}")
hf_gen.close()

print("Running VOI")
metrics = rand_voi.rand_voi(gt, gen)
print("Done")
print(f"Metrics: {metrics}")
print(f"Total VOI: {metrics['voi_split'] + metrics['voi_merge']}")