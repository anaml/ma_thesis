from skimage.measure import label
from pathlib import Path
import h5py
import numpy as np

version = 'v25'
nr = '931832'

unet_segm_path = Path(f"/cajal/scratch/users/anaml/3k_65000n_v3_test/unet_inference_957814/semseg_22-12-14_15-38-24-957814_{version}_{nr}.hdf5")
save_path_instance_segm = Path(f"/cajal/scratch/users/anaml/3k_65000n_v3_test/instance_segm_957814/instance_segm_22-12-14_15-38-24-957814_{version}_{nr}.hdf5")
print("Loading data")
hf1 = h5py.File(str(unet_segm_path), "r")
segm = hf1['label_values'][()]
hf1.close()
print("Data loaded")
print("Start labeling the semantic segmentation")
instance_segm, num = label(segm, background=None, return_num=True, connectivity=None)
print("Done")
print("Saving instance segmentation")
with h5py.File(str(save_path_instance_segm), 'w') as f:
    f.create_dataset("label_values", data=instance_segm, dtype=np.uint32)
    f.close()
print("Saved")
